- platform: cert_expiry
  host: !secret arbiter_host
  port: !secret arbiter_port
  name: ssl_arbiter

- platform: cert_expiry
  host: !secret genesis_host
  port: !secret genesis_port
  name: ssl_genesis

- platform: cert_expiry
  host: !secret infinity_host
  port: !secret infinity_port
  name: ssl_infinity

- platform: cert_expiry
  host: !secret witness_host
  port: !secret witness_port
  name: ssl_witness

- platform: command_line
  name: ZoneMinder State
  command: "/home/homeassistant/.homeassistant/bin/zm_state.py --get"
  scan_interval: 5

- platform: filesize
  file_paths:
    - !secret homeassistant_db_file

- platform: gitlab_ci
  name: GitLab CI Status
  gitlab_id: !secret gitlab_project_id
  token: !secret gitlab_token

- platform: pi_hole
  host: !secret pihole_ip
  monitored_conditions:
    - ads_blocked_today
    - ads_percentage_today
    - dns_queries_today
    - unique_clients
    - domains_being_blocked

- platform: qnap
  host: !secret qnap_ip
  port: !secret qnap_port
  ssl: true
  verify_ssl: false
  username: !secret qnap_user
  password: !secret qnap_pass
  monitored_conditions:
    - system_temp
    - cpu_temp
    - cpu_usage
    - memory_percent_used
    - volume_percentage_used

- platform: rest
  name: Upstream Version
  resource: https://pypi.python.org/pypi/homeassistant/json
  value_template: '{{ value_json.info.version }}'
  scan_interval: 3600

- platform: systemmonitor
  resources:
    - type: disk_use_percent
      arg: /
    - type: disk_use_percent
      arg: /var/cache/zoneminder/
    - type: memory_use_percent
    - type: processor_use
    - type: last_boot

- platform: template
  sensors:
    guilty_spark:
      value_template: '{% if is_state("device_tracker.google_maps_106743639687882572738", "not_home") %}Away{% elif is_state("device_tracker.google_maps_106743639687882572738", "home") %}Home{% else %}{{ states("device_tracker.google_maps_106743639687882572738") }}{% endif %}'
    guilty_spark_battery_level:
      value_template: '{{ state_attr("device_tracker.google_maps_106743639687882572738", "battery_level") }} %'
      icon_template: '{% if state_attr("device_tracker.google_maps_106743639687882572738", "battery_charging") == True %}{% if state_attr("device_tracker.google_maps_106743639687882572738", "battery_level") < 10 %}mdi:battery-charging-10{% elif state_attr("device_tracker.google_maps_106743639687882572738", "battery_level") < 20 %}mdi:battery-charging-20{% elif state_attr("device_tracker.google_maps_106743639687882572738", "battery_level") < 30 %}mdi:battery-charging-30{% elif state_attr("device_tracker.google_maps_106743639687882572738", "battery_level") < 40 %}mdi:battery-charging-40{% elif state_attr("device_tracker.google_maps_106743639687882572738", "battery_level") < 50 %}mdi:battery-charging-50{% elif state_attr("device_tracker.google_maps_106743639687882572738", "battery_level") < 60 %}mdi:battery-charging-60{% elif state_attr("device_tracker.google_maps_106743639687882572738", "battery_level") < 70 %}mdi:battery-charging-70{% elif state_attr("device_tracker.google_maps_106743639687882572738", "battery_level") < 80 %}mdi:battery-charging-80{% elif state_attr("device_tracker.google_maps_106743639687882572738", "battery_level") < 90 %}mdi:battery-charging-90{% elif state_attr("device_tracker.google_maps_106743639687882572738", "battery_level") < 100 %}mdi:battery-charging-100{% endif %}{% else %}{% if state_attr("device_tracker.google_maps_106743639687882572738", "battery_level") == 100 %}mdi:battery{% elif state_attr("device_tracker.google_maps_106743639687882572738", "battery_level") > 90 %}mdi:battery-90{% elif state_attr("device_tracker.google_maps_106743639687882572738", "battery_level") > 80 %}mdi:battery-80{% elif state_attr("device_tracker.google_maps_106743639687882572738", "battery_level") > 70 %}mdi:battery-70{% elif state_attr("device_tracker.google_maps_106743639687882572738", "battery_level") > 60 %}mdi:battery-60{% elif state_attr("device_tracker.google_maps_106743639687882572738", "battery_level") > 50 %}mdi:battery-50{% elif state_attr("device_tracker.google_maps_106743639687882572738", "battery_level") > 40 %}mdi:battery-40{% elif state_attr("device_tracker.google_maps_106743639687882572738", "battery_level") > 30 %}mdi:battery-30{% elif state_attr("device_tracker.google_maps_106743639687882572738", "battery_level") > 20 %}mdi:battery-20{% elif state_attr("device_tracker.google_maps_106743639687882572738", "battery_level") > 10 %}mdi:battery-10{% else %}mdi:battery-outline{% endif %}{% endif %}'
      friendly_name: Pixel 3 XL
    guilty_spark_battery_state:
      value_template: '{% if state_attr("device_tracker.google_maps_106743639687882572738", "battery_charging") == True %}Charging{% else %}Unplugged{% endif %}'
      icon_template: '{% if state_attr("device_tracker.google_maps_106743639687882572738", "battery_charging") == True %}mdi:power-plug{% else %}mdi:power-plug-off{% endif %}'
      friendly_name: Charge
    cortana:
      value_template: '{% if is_state("device_tracker.cortana", "not_home") %}Away{% elif is_state("device_tracker.cortana", "home") %}Home{% else %}{{ states("device_tracker.cortana") }}{% endif %}'
    alarm:
      value_template: >-
        {% if is_state('input_boolean.alarm', 'on') %}
          on
        {% else %}
          off
        {% endif %}
      icon_template: >-
        {% if is_state('input_boolean.alarm', 'on') %}
          mdi:bell-ring
        {% else %}
          mdi:bell-outline
        {% endif %}
    front_door:
      value_template: >-
        {% if is_state('binary_sensor.sensor_2_contact', 'on') %}
          open
        {% else %}
          closed
        {% endif %}
      icon_template: >-
        {% if is_state('binary_sensor.sensor_2_contact', 'on') %}
          mdi:door-open
        {% else %}
          mdi:door-closed
        {% endif %}
    side_door:
      value_template: >-
        {% if is_state('binary_sensor.sensor_1_contact', 'on') %}
          open
        {% else %}
          closed
        {% endif %}
      icon_template: >-
        {% if is_state('binary_sensor.sensor_1_contact', 'on') %}
          mdi:door-open
        {% else %}
          mdi:door-closed
        {% endif %}
    back_door:
      value_template: >-
        {% if is_state('binary_sensor.sensor_3_contact', 'on') %}
          open
        {% else %}
          closed
        {% endif %}
      icon_template: >-
        {% if is_state('binary_sensor.sensor_3_contact', 'on') %}
          mdi:door-open
        {% else %}
          mdi:door-closed
        {% endif %}
    system_state_template:
      value_template: "{{ states.input_select.system.state }}"
      icon_template: >-
        {% if is_state('input_select.system', 'Vacation') %}
          mdi:airplane
        {% elif is_state('input_select.system', 'Guest') %}
          mdi:account-group
        {% else %}
          mdi:home
        {% endif %}
    zoneminder_state_template:
      value_template: "{{ states.sensor.zoneminder_state.state }}"
      icon_template: >-
        {% if is_state('alarm_control_panel.home', 'armed_home') %}
          mdi:bell-plus
        {% elif is_state('alarm_control_panel.home', 'armed_away') %}
          mdi:bell
        {% else %}
          mdi:bell-outline
        {% endif %}
    office_status_template:
      value_template: "{{ states.sensor.office_status.state }}"
      icon_template: >-
        {% if is_state('sensor.office_status', 'Monitor') %}
          mdi:camcorder-off
        {% elif is_state('sensor.office_status', 'Nodect') %}
          mdi:camcorder
        {% else %}
          mdi:help
        {% endif %}

- platform: version
  name: Version

- platform: zoneminder

import json, logging, requests
import cookielib, urllib2

class ZoneMinder:
    def __init__(self, ip, port, username, password):
        self.ip = ip
        self.port = port
        self.username = username
        self.password = password
        self.logger = logging.getLogger()
        self.session = requests.Session()
        self.__login()

    def __login(self):
        url = "https://{}:{}/zm/index.php".format(self.ip,self.port)
        params = {
            "username": self.username,
            "password": self.password,
            "action": "login",
            "view": "console"
        }
        result = self.session.post(url, data=params)

    def getAllStates(self):
        url = "https://{}:{}/zm/api/states.json".format(self.ip,self.port)
        result = self.session.get(url).json()
        self.logger.debug(result)
        return result['states']

    def getCurrentState(self):
        url = "https://{}:{}/zm/api/states.json".format(self.ip,self.port)
        result = self.session.get(url).json()
        self.logger.debug(result)
        for state in result['states']:
            if int(state['State']['IsActive']):
                return state['State']['Name']

    def setState(self, state):
        url = "https://{}:{}/zm/api/states/change/{}.json".format(self.ip,self.port,state)
        result = self.session.post(url)
        self.logger.debug("SETTING STATE: {}".format(result))

    def clearAlarm(self, mid):
        url = "https://{}:{}/zm/api/monitors/alarm/id:{}/command:off.json".format(self.ip,self.port,mid)
        result = self.session.get(url)
        self.logger.debug(result)
        return result

    def triggerAlarm(self, mid):
        url = "https://{}:{}/zm/api/monitors/alarm/id:{}/command:on.json".format(self.ip,self.port,mid)
        result = self.session.get(url)
        self.logger.debug(result)
        return result

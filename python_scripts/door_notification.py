door = data.get('door')
door_state = data.get('state')
downstairs_state = hass.states.get('media_player.downstairs').state
kitchen_speaker_state = hass.states.get('media_player.kitchen_speaker').state
living_room_speaker_state = hass.states.get('media_player.living_room_speaker').state
silent = hass.states.get('input_boolean.silent').state
alarm_state = hass.states.get('alarm_control_panel.home').state
if door != None:
    if door_state == "on":
        door_state = "opened"
        if alarm_state == "armed_home" or alarm_state == "armed_away":
            hass.services.call("media_player", "volume_set", {"entity_id": "media_player.kitchen_speaker", "volume_level": "0.5"})
            hass.services.call("tts", "google_translate_say", {"entity_id": "media_player.kitchen_speaker", "message": "{} door. Disarm system within 30 seconds".format(door)})
            hass.services.call("media_player", "volume_set", {"entity_id": "media_player.living_room_speaker", "volume_level": "0.5"})
            hass.services.call("tts", "google_translate_say", {"entity_id": "media_player.living_room_speaker", "message": "{} door. Disarm system within 30 seconds".format(door)})
        elif silent.lower() != "on":
            if downstairs_state.lower() != "playing" and kitchen_speaker_state.lower() != "playing" and living_room_speaker_state.lower() != "playing":
                hass.services.call("media_player", "volume_set", {"entity_id": "media_player.kitchen_speaker", "volume_level": "0.5"})
                hass.services.call("tts", "google_translate_say", {"entity_id": "media_player.kitchen_speaker", "message": "{} door".format(door)})
                hass.services.call("media_player", "volume_set", {"entity_id": "media_player.living_room_speaker", "volume_level": "0.5"})
                hass.services.call("tts", "google_translate_say", {"entity_id": "media_player.living_room_speaker", "message": "{} door".format(door)})
            elif kitchen_speaker_state.lower() != "playing" and downstairs_state.lower() != "playing":
                hass.services.call("media_player", "volume_set", {"entity_id": "media_player.kitchen_speaker", "volume_level": "0.5"})
                hass.services.call("tts", "google_translate_say", {"entity_id": "media_player.kitchen_speaker", "message": "{} door".format(door)})
            elif living_room_speaker_state.lower() != "playing" and downstairs_state.lower() != "playing":
                hass.services.call("media_player", "volume_set", {"entity_id": "media_player.living_room_speaker", "volume_level": "0.5"})
                hass.services.call("tts", "google_translate_say", {"entity_id": "media_player.living_room_speaker", "message": "{} door".format(door)})
    else:
        door_state = "closed"
    if alarm_state.lower() != "disarmed" and alarm_state.lower() != "pending" and door_state.lower() != "closed":
        hass.services.call("notify", "html5", {"title" : "{} door {}".format(door, door_state), "message" : "System is set to {}".format(alarm_state)})

entity = data.get('entity')
entity_name = data.get('entity_name')
entity_to_state = str.lower(data.get('entity_to_state'))
entity_from_state = str.lower(data.get('entity_from_state'))
alarm_state = str.lower(hass.states.get('alarm_control_panel.home').state)
system_state = str.lower(hass.states.get('input_select.system').state)
cortana_state = str.lower(hass.states.get('sensor.cortana').state)
guilty_spark_state = str.lower(hass.states.get('sensor.guilty_spark').state)

hass.services.call("notify", "glados", {"title" : "Presence Detection", "message" : "_{}'s location changed to {}_".format(entity_name, entity_to_state)})
if entity_to_state == "home" and alarm_state != "disarmed":
    # ARRIVED HOME
    hass.services.call("script", "disarm_alarm")
    hass.services.call("notify", "glados", {"title" : "Alarm State", "message" : "_Alarm automatically disarmed_"})
    if entity == "sensor.cortana":
        hass.services.call("notify", "ios_cortana", {"title" : "Arrived home", "message" : "System automatically disarmed"})
        hass.services.call("notify", "html5_guilty_spark", {"title" : "Melinda arrived home", "message" : "System automatically disarmed"})
    elif entity == "sensor.guilty_spark":
        hass.services.call("notify", "html5_guilty_spark", {"title" : "Arrived home", "message" : "System automatically disarmed"})
        hass.services.call("notify", "ios_cortana", {"title" : "Nick arrived home", "message" : "System automatically disarmed"})
    if system_state == "vacation":
        hass.services.call("input_select", "select_option", {"entity_id" : "input_select.system", "option" : "Normal"})
        hass.services.call("notify", "glados", {"title" : "System Mode", "message" : "_System mode changed from Vacation back to Normal_"})
elif entity_to_state == "away" and entity_from_state == "home" and alarm_state == "disarmed":
    # LEFT HOME
    if cortana_state != "home" and guilty_spark_state != "home":
        if system_state == "guest":
            if entity == "sensor.cortana":
                service_data = {
                                    "title" : "Arm system to home or away?",
                                    "message" : "System not automatically armed due to guest mode",
                                    "data" :
                                    {
                                        "push" :
                                            {"category" : "iosarmhomeaway"}
                                    }
                                }
                hass.services.call("notify", "ios_cortana", service_data)
                service_data = {
                                    "title" : "Melinda left home",
                                    "message" : "System not armed due to guest mode"
                                }
                hass.services.call("notify", "html5_guilty_spark", service_data)
            elif entity == "sensor.guilty_spark":
                service_data = {
                                    "title" : "Arm system to home or away?",
                                    "message" : "System not automatically armed due to guest mode",
                                    "data" :
                                    {
                                        "actions" :
                                            [{"action" : "notification_response_arm_home", "title" : "Home"}, {"action" : "notification_response_arm_away", "title" : "Away"}]
                                    }
                                }
                hass.services.call("notify", "html5_guilty_spark", service_data)
                service_data = {
                                    "title" : "Nick left home",
                                    "message" : "System not armed due to guest mode"
                                }
                hass.services.call("notify", "ios_cortana", service_data)
        else:
            hass.services.call("script", "arm_alarm_home")
            hass.services.call("notify", "glados", {"title" : "Alarm State", "message" : "_Alarm automatically armed home_"})
            if entity == "sensor.cortana":
                service_data = {
                                    "title" : "System automatically armed home",
                                    "message" : "Is Penny with you?",
                                    "data" :
                                    {
                                        "push" :
                                            {"category" : "iosarmaway"}
                                    }
                                }
                hass.services.call("notify", "ios_cortana", service_data)
                service_data = {
                                    "title" : "Melinda left home",
                                    "message" : "System automatically armed to home"
                                }
                hass.services.call("notify", "html5_guilty_spark", service_data)
            elif entity == "sensor.guilty_spark":
                service_data = {
                                    "title" : "System automatically armed home",
                                    "message" : "Is Penny with you?",
                                    "data" :
                                    {
                                        "actions" :
                                            [{"action" : "notification_response_arm_away", "title" : "Yes"}, {"action" : "ignore_notification", "title" : "No"}]
                                    }
                                }
                hass.services.call("notify", "html5_guilty_spark", service_data)
                service_data = {
                                    "title" : "Nick left home",
                                    "message" : "System automatically armed to home"
                                }
                hass.services.call("notify", "ios_cortana", service_data)
elif entity_to_state == "away" and entity_from_state == "work" and entity == "sensor.guilty_spark":
    # LEFT WORK
    hass.services.call("notify", "ios_cortana", {"title" : "Location update", "message" : "Nick left work"})
elif entity_to_state == "work" and entity == "sensor.guilty_spark":
    # ARRIVED WORK
    hass.services.call("notify", "ios_cortana", {"title" : "Location update", "message" : "Nick arrived at work"})

# Home Assistant Configuration
[Home Assistant](https://home-assistant.io/) is an open-source home automation platform running on Python 3, designed to track, control, and automate all devices at home. This repository contains the majority of the configurations for my instance of Home Assistant, excluding any sensitive data.

## Extra Components
In addition to the configurations present in this repo, several additional components are at play in the environment. While those configurations aren't exposed here (yet), the following list will serve as a placeholder in the interim.
* Appdaemon - [GitHub](https://github.com/home-assistant/appdaemon) / [Docs](http://appdaemon.readthedocs.io/en/latest/)
* HaDashboard - [Home Assistant Docs](https://www.home-assistant.io/docs/ecosystem/hadashboard/) / [HaDashboard Docs](https://appdaemon.readthedocs.io/en/stable/DASHBOARD_INSTALL.html)
* Occusim - [GitHub](https://github.com/acockburn/occusim)
* (REMOVED) Homebridge - [GitHub](https://github.com/home-assistant/homebridge-homeassistant)
* (REMOVED) SmartThings MQTT Bridge - [Home Assistant Blog](https://home-assistant.io/blog/2016/02/09/Smarter-Smart-Things-with-MQTT-and-Home-Assistant/)
 * _Replaced with native SmartThings component - [SmartThings Component](https://www.home-assistant.io/components/smartthings/)_
